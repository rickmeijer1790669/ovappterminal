package adsd.semester2team1.model;

import java.time.LocalDate;

public class Traject {
  private String vertrekpunt;
  private String aankomstpunt;
  private long vertrekTijd;
  private long aankomstTijd;
  private LocalDate datum;
  private float kosten;
  private Vervoersmiddel vervoersmiddel;

  public Traject(String vertrekpunt, String aankomstpunt, long vertrekTijd, long aankomstTijd, LocalDate datum, float kosten, Vervoersmiddel vervoersmiddel) {
    this.vertrekpunt = vertrekpunt;
    this.aankomstpunt = aankomstpunt;
    this.vertrekTijd = vertrekTijd;
    this.aankomstTijd = aankomstTijd;
    this.datum = datum;
    this.kosten = kosten;
    this.vervoersmiddel = vervoersmiddel;
  }

  public String getVertrekpunt() {
    return vertrekpunt;
  }

  public void setVertrekpunt(String vertrekpunt) {
    this.vertrekpunt = vertrekpunt;
  }

  public String getAankomstpunt() {
    return aankomstpunt;
  }

  public void setAankomstpunt(String aankomstpunt) {
    this.aankomstpunt = aankomstpunt;
  }

  public long getVertrekTijd() {
    return vertrekTijd;
  }

  public void setVertrekTijd(long vertrekTijd) {
    this.vertrekTijd = vertrekTijd;
  }

  public long getAankomstTijd() {
    return aankomstTijd;
  }

  public void setAankomstTijd(long aankomstTijd) {
    this.aankomstTijd = aankomstTijd;
  }

  public LocalDate getDatum() {
    return datum;
  }

  public void setDatum(LocalDate datum) {
    this.datum = datum;
  }

  public float getKosten() {
    return kosten;
  }

  public void setKosten(float kosten) {
    this.kosten = kosten;
  }

  public Vervoersmiddel getVervoersmiddel() {
    return vervoersmiddel;
  }

  public void setVervoersmiddel(Vervoersmiddel vervoersmiddel) {
    this.vervoersmiddel = vervoersmiddel;
  }
}
