package adsd.semester2team1.model;

public class Vervoersmiddel {
  private String naam;

  public String getNaam() {
    return naam;
  }

  public void setNaam(String naam) {
    this.naam = naam;
  }

  public Vervoersmiddel(String naam) {
    this.naam = naam;
  }
}
