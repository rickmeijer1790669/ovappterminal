package adsd.semester2team1;

import adsd.semester2team1.model.Traject;
import adsd.semester2team1.model.Vervoersmiddel;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

  private static final List<Traject> trajectList = new ArrayList<Traject>();
  private static final Scanner scanner = new Scanner(System.in);

  public static void initialize() {
    // Create calendar object
    Calendar cal = Calendar.getInstance();
    trajectList.add(new Traject("Amersfoort", "Deventer", cal.getTimeInMillis() - (30 * 60 * 1000), cal.getTimeInMillis() - (10 * 60 * 1000), LocalDate.now(), (float) 12.49, new Vervoersmiddel("trein")));
    trajectList.add(new Traject("Amersfoort", "Deventer", cal.getTimeInMillis() - (7 * 60 * 1000), cal.getTimeInMillis() + (17 * 60 * 1000), LocalDate.now(), (float) 15.49, new Vervoersmiddel("trein")));
    trajectList.add(new Traject("Amersfoort", "Deventer", cal.getTimeInMillis() + (30 * 60 * 1000), cal.getTimeInMillis() + (50 * 60 * 1000), LocalDate.now(), (float) 12.49, new Vervoersmiddel("trein")));
    trajectList.add(new Traject("Amersfoort", "Deventer", cal.getTimeInMillis() + (7 * 60 * 1000), cal.getTimeInMillis() + (27 * 60 * 1000), LocalDate.now(), (float) 15.49, new Vervoersmiddel("trein")));
    trajectList.add(new Traject("Amersfoort", "Apeldoorn", cal.getTimeInMillis() + (15 * 60 * 1000), cal.getTimeInMillis() + (35 * 60 * 1000), LocalDate.now(), (float) 4.49, new Vervoersmiddel("trein")));
  }

  public static String getTrajectOutputString(Traject traject) {
    var formatter = new SimpleDateFormat("HH:mm");
    var vertrekTijd = formatter.format(new Date(traject.getVertrekTijd()));
    var aankomstTijd = formatter.format(new Date(traject.getAankomstTijd()));
    return String.format("De %s naar %s vanaf %s vertrekt om %s, komt aan om %s en kost %.2f", traject.getVervoersmiddel().getNaam(), traject.getAankomstpunt(), traject.getVertrekpunt(), vertrekTijd, aankomstTijd, traject.getKosten());
  }


  public static void main(String[] args) {
    initialize();
    System.out.println("Vul uw vertrekpunt in:");
    String vertrekpunt = scanner.nextLine();

    System.out.println("Vul uw aankomstpunt in:");
    String aankomstpunt = scanner.nextLine();

    System.out.println("Vul uw vertrektijd in:");
    String vertrekTijd = scanner.nextLine();
    // Split input time to separate hours and minutes
    String[] splitVertrekTijd = vertrekTijd.split(":");
    // Create calendar object
    Calendar cal = Calendar.getInstance();
    // Set current time and date to calendar object
    cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(splitVertrekTijd[0]));
    cal.set(Calendar.MINUTE, Integer.parseInt(splitVertrekTijd[1]));
    long vertrekTijdEpoch = cal.getTimeInMillis();

    long vertrekTijdPlus30Min = vertrekTijdEpoch + (30 * 60 * 1000);
    long vertrekTijdMinus30Min = vertrekTijdEpoch - (30 * 60 * 1000);

    List<Traject> listOfCompatibleTrajecten = trajectList.stream().filter(t -> t.getVertrekpunt().equals(vertrekpunt) &&
        t.getAankomstpunt().equals(aankomstpunt) && (t.getVertrekTijd() >= vertrekTijdMinus30Min && t.getVertrekTijd() <= vertrekTijdPlus30Min))
        .sorted(Comparator.comparingLong(Traject::getVertrekTijd)).collect(Collectors.toList());

    if (!listOfCompatibleTrajecten.isEmpty()) {
      for (var traject : listOfCompatibleTrajecten) {
        System.out.println(getTrajectOutputString(traject));
      }
    } else {
      System.out.println("Sorry! Geen bestaande trajecten voor deze combinatie.");
    }
  }
}
